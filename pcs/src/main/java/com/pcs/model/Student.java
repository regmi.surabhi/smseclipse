package com.pcs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="student")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@NotEmpty(message = "Name Empty")
	@Column(name = "Name", nullable = false)
	private String Name;
	
	@NotEmpty(message = "Email Empty")
	@Column(name = "Email", nullable = false)
	private String Email ;
	
	@NotEmpty(message = "Address Empty")
	@Column(name = "Address", nullable = false)
	private String Address;
	
	@NotEmpty(message = "Phone Empty")
	@Column(name = "Phone", nullable = false)
	private String Phone;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
 
}
