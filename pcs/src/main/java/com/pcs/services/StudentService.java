package com.pcs.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.pcs.model.Student;
@Service
public interface StudentService {
	public List<Student> studentList();
	public Student saveStudent(Student student);
	public Student updateStudent(Student student);
	public Student getStudent(int id);
	public Student deleteStudent(int id);

}
