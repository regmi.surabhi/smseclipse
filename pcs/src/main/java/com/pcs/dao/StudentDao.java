package com.pcs.dao;

import java.util.List;

import com.pcs.model.Student;

public interface StudentDao {
	public List<Student> studentList();
	public Student saveStudent(Student student);
	public Student getStudent(int id);
	public Student updateStudent(Student student);
	public Student deleteStudent(int id);
}
